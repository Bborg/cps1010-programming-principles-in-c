//
// Created by benja on 04/11/2019.
//

#include <stdio.h>
#include <stdbool.h>


#define Num_Inputs 5
int main(void) {


    double arr[Num_Inputs];
    double tot = 0;
    double mean;
    double mode;
    double median;
    int c=0;
    double input;
    double currentMax=0;
    bool nonUnique = false;



    while (c<Num_Inputs)
    {

    printf("Enter Number(-1 to quit):");
    scanf("%lf", &input);
    if (input == -1)
    {
        break;//leaves the loop
    }
    if (input < 1 || input > 100)
    {
        printf("\nrange between 1 and 100\n");
        continue; // next iteration of the loop
    }
    if (input < currentMax)//checks that numbers entered are in ascending order
    {
        printf("\nInputs must be ascending\n");
        continue;
    }
    if (input == currentMax)
    {
        nonUnique= true;
    }
    arr[c]=input;
    currentMax=input;
    tot = arr[c] + tot;
    c++;//increments loop
    if (c==Num_Inputs-1 && !nonUnique )
    {
        arr[c]=currentMax;
        tot =arr[c]+tot;
        break;
    }
      }

    for (int i =0; i<Num_Inputs;i++)
    {
        printf("%lf ",arr[i]);
    }

mean = tot/Num_Inputs;

/*Median*/
if ((sizeof(arr)/8)%2==0) //sizeof(arr)/8 because double slots take up 8bytes and int take up 1
{
    median = ((arr[(sizeof(arr)/8)/2]+arr[((sizeof(arr)/8)/2-1)])/2);
}else{
    median = arr[((sizeof(arr)/8)/2)];
}

/* Mode using Mean-Mode = 3[Mean-Median]*/
mode = mean - 3*(mean-median);

printf("\nMean: %f \nMedian: %f\nMode: %f",mean, median,mode);
}

