//
// Created by benja on 31/10/2019.
//
#include <stdio.h>
#include <math.h>

#define  s  5

int main(void)
{
    float tot=0;
    for (int i=1;i<=5;++i)
    {
        tot = tot +1/(pow(i,s));
    }
    printf("%.4f", tot);
    return 0;
}
