//
// Created by benja on 14/12/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define NAME_LEN 20
#define NO_STRINGS 20
#define NO_CHARS 10

char * search(char (*arr)[NO_CHARS],char *word);

int main (void)
{
    FILE *fp;
    char fileName[NAME_LEN];
    char arr[100];
    char arr1[NO_STRINGS][NO_CHARS];
    int ch;
    int i=0,tot=0;
    int k=0;
    char word[NO_CHARS];

    printf("Enter file name.\n");
    scanf("%20s", fileName);

    if ((fp = fopen(fileName,"r+")) == NULL)
    {
        printf("Can't open file.\n");

        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(fp)) != EOF) {

        arr[i] = (char)ch;
        i++;
    }
     arr[i]='\0';
    tot =i;

    for (int i=0 ; i<NO_STRINGS;i++)
    {
        if (arr[k]=='\0')
        {
            break;
        }
        for (int j=0;j<NO_CHARS;j++)
        {

            if (isspace(arr[k]))
            {
                arr1[i][j]='\0';
                k++;
                break;
            }else {
                arr1[i][j] = arr[k];
                k++;
            }
        }
    }

  strcpy(word,search(arr1,word));
printf("\n%s",word);
}

char * search(char (*arr)[NO_CHARS],char *word)
{

    printf("\nsearch: ");
    scanf("%s", word );

    for (int i=0; i<NO_STRINGS;i++)
    {
        if(strcmp(arr[i],word)==0)
        {
            return word;
        }else{
            continue;
        }
        return "\0";
    }

}