//
// Created by benja on 3/3/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>
int y=0;

int fibonacci(int n);
int fibSum(int n);

int main ()
{
    int n ;

    printf("Enter the value for n.\n\n");
    scanf("%d",&n);

    printf("The fibonacci sequence up to n = %d: ",n);

        printf("%d ",fibonacci(n));


}

int fibonacci(int n)
{
    if (n==0)
    {
        return 0;
    }else if(n==1)
    {
        return 1;
    }
    //Above if and else if statement, return the base case values.

    int arr[n+1]; //array used for storing the values of the fib sequence to be summed.
    arr[0]=0;
    arr[1]=1;
    int fib=1;//initialising sum variable.

    for (int i=2;i<=n;i++)
    {
        arr[i]=arr[i-1]+arr[i-2];
        fib = fib + arr[i];
    }//for loop generates new values for the fib sequence and sums.

    return fib;
}

