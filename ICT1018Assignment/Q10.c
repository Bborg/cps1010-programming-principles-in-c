//
// Created by benja on 4/9/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

#define ARR_LEN 15

void generate(int *seq, int length);
int maxElement(int *arr, int lenth);
int isGreater(int x,int y);

int main()
{
    int maxElementVar;
    int arr[ARR_LEN];
    generate(arr,ARR_LEN);

    maxElementVar=maxElement(arr,ARR_LEN);

    printf("Largest Element in given array is: %d.", maxElementVar);
}

void generate(int *seq, int length)
{
    for (int i=0; i<length; i++)//For loop traverses the array.
    {

        seq[i]= (rand()%1025);//assigns random number between 0-1024 to all entries in the array.
    }
}

int maxElement(int *arr, int length)
{
 if(length ==1)
     return arr[0];

 return isGreater(arr[length-1],maxElement(arr,length-1));
}

int isGreater(int x,int y)
{
    if(x > y)
    {
        return x;
    }else if(y>x) {
        return y;
    }
}