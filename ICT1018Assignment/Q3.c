//
// Created by benja on 3/2/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

int main ()
{
 int arr[]={9,7,6,4,2};
 bool sorted = true;
 for (int i =1; i< ((sizeof(arr)/4)-1);i++) //i=1 and sizeof() -1 to exclude first and last elements
 {
     if (arr[i]>arr[i+1] && arr[i]>arr[i-1]) // If condition checking for extreme points in array.
     {
        printf("Extreme Point, %d\n", arr[i]);
        sorted = false;
     }
 }
 if (sorted == true)
     printf("SORTED\n");
}

/*An array of integers is considered sorted if for all elements of the array say A, A[i-1] <= A[i] <= A[i+1],
 *in the case that is is sorted in ascending order,or A[i-1]>=A[i]>=A[i+1] in the case that is is sorted in
 * descending order. Hence an array with extreme points cannot be SORTED. */
