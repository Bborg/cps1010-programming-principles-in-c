//
// Created by benja on 4/9/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

#define SHELL_LEN 15
#define QUICK_LEN 20

#define MERGE_LEN SHELL_LEN + QUICK_LEN //Length for the final array in the merge function derived from the two defined lengths in Q1.

void ShellSort(int* arr, int arrSize);
void quickSort(int arr[], int low,int high);
void merge(int arr1[],int arr2[], int size1, int size2);//Merge sorts two sorted arrays.
void generate(int *seq, int length);

int main()
{
    int arrA[SHELL_LEN] ;
    int arrB[QUICK_LEN];

    generate(arrA,SHELL_LEN);
    generate(arrB,QUICK_LEN);

    quickSort(arrB, 0,QUICK_LEN-1);
    printf("Quick Sort on Array B: ");
    for (int i=0;i<QUICK_LEN;i++)
    {
        printf("%d ",arrB[i]);
    }
    printf("\n");
    printf("\n");
    ShellSort(arrA,SHELL_LEN);

    merge(arrA,arrB,SHELL_LEN,QUICK_LEN);
}

void ShellSort(int* arr, int arrSize)
{
    for (int gap = arrSize/2; gap>0; gap/=2) //Sets gap to (size of the array)/2 initially, then divides gap by 2 at each iteration until gap=0;
    {
        for (int i = gap; i<arrSize; i++)//For loop for comparing the elements in the array seperated by the gap sepcified above.
        {
            int temp = arr[i];//Variable for temporarily storing the element at position [i].
            int j; //variable for below for loop initialised outside since it is re used outside of the for loop.

            for (j=i; j>= gap && arr[j-gap]>temp;j-=gap)
                /*For loop that acts more like a for loop and an if condition combined into one.
 *                Checks to see if the left most element being compared ie element @[j-gap] is greater than that @ [i] (or variable temp)
                 *If so the elements are swapped.              */
            {
                arr[j] = arr[j-gap];
            }
            arr[j] = temp;
        }
    }

    printf("Shell Sort on Array A: ");
    for (int i=0;i<SHELL_LEN;i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
    printf("\n");
}

void swap(int* x, int* y)
{
    int t = *x;
    *x = *y;
    *y = t;
}

int partition(int arr[], int low, int high)
{
    int pivot = arr[high];
    int i = (low -1);

    for (int j = low; j<=(high-1);j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap (&arr[i+1], &arr[high]);
    return (i+1);
}

void quickSort(int arr[], int low,int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }


}

/*Assuming the two arrays taken as inputs are sorted*/
void merge(int arr1[],int arr2[], int size1, int size2) {


    int final[MERGE_LEN];// New array with size of the two original arrays combined
    int i=0;//counter for, for loop.
    int arr1p=i; //Keeps track of where we got to in array1.
    int arr2p=i; //Keeps track of where we got to in array2.
    int shift = 0; //Keeps track of current position in final array.

    for (i=0;i<MERGE_LEN;i++)
    {
        if(shift>=MERGE_LEN) //If the array locations of final are exceded by shift loop breaks
        {
            break;
        }

        if(arr1p>=size1)/*If the end of array1 is reached by arr1p, the remaining elements in array2 are simply appended
 *                        to the back of the final array. Once completed we break out of the original for loop.*/
        {
            for(i;i<MERGE_LEN;i++)
            {
                final[shift]=arr2[arr2p];
                shift++;
                arr2p++;
            }
            break;
        }else if (arr2p>=size2)/*If the end of array2 is reached by arr2p, the remaining elements in array1 are appended
 *                               to the back of the final array. Once completed we break out of the original for loop.*/{
            for(i;i<MERGE_LEN;i++) {
                final[0 + shift] = arr1[arr1p];

                shift += 1;
                arr1p += 1;
            }
            break;
        }


        if(arr1[arr1p]<=arr2[arr2p]) //Before arr1p or arr2p excede the length of their respective arrays.
                                     //The current elements in each array as indicated by the two above mentioned int values
                                     //are compared and slotted into the final array accordingly.

        {
            final[0+shift]=arr1[arr1p];
            shift+=1;
            arr1p+=1;
        }else{
            final[0+shift]=arr2[arr2p];
            shift+=1;
            arr2p+=1;
        }


    }


    printf("MERGE SORT on Array A and B: "); //The newly merge sorted final array is printed.
    for (int i=0;i<MERGE_LEN;i++)
    {
        printf("%d ",final[i]);
    }
    printf("\n");



}

void generate(int *seq, int length)
{

    for (int i=0; i<length; i++)//For loop traverses the array.
    {

        seq[i]= (rand()%1025);//assigns random number between 0-1024 to all entries in the array.
    }

}
