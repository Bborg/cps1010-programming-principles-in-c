//
// Created by benja on 3/1/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

double newtonRaphson(double n, int iterations);
double fprime(double t);
double f(double n,double t);

int main ()
{
   printf("%f ", newtonRaphson(15,100)) ;
}

double f(double n,double t) //function for computing the value given by the original function
{
    double x = t*t - n;

    return x;
}

double fprime(double t)// Function for computing the value given by the derivative of the original function
{
    double x = 2*t;

    return x;
}

double newtonRaphson(double n, int iterations)
{
    double x=2;
    double y;

    for (int i =0; i<iterations;i++)// For loop allows for implementation of the newton raphson method for approximating roots of eqns.
    {
        y= x-(f(n,x)/fprime(x));
        x=y;
    }

    return y;

}