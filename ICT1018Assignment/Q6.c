//
// Created by benja on 2/28/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

bool isPrime(int n);//Checks if number passed as an argument is prime.
void generate(int *seq, int x);//Generates a sequence of integers in an array passed as an argument.
void sieveOfEratosthenes();//Algorithm traverses the array zeroing out any multiples of prime numbers.

int main ()
{
 sieveOfEratosthenes();
}


bool isPrime(int n)
{
    if (n == 1)
    {
        return false;
    }

    if (n%2 == 0 && n != 2)
    {
        return false;
    }else{
        for (int i =2; i <= sqrt(n); i++)
        {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}

void sieveOfEratosthenes()
{
    int n;
    printf("Enter the upperbound of desired sequence.\n");
    scanf("%d", &n);

    int arr[n];

    generate(arr,n);


    for (int i = 0; (arr[i]*arr[i]) <= (n);i++)
    {
        if(isPrime(i) == true)
        {
            for (int j=(arr[i]*arr[i]); j<=(n);j+=arr[i])
                arr[j] = 0;
        }
    }

    printf("Output: ");
    for (int i=2; i< sizeof(arr)/4; i++)
    {
        if (arr[i]==0)
        {
            continue;
        }
        else
        {
            printf("%d ", arr[i]);
        }
    }
}

void generate(int *seq, int x)
{

    for (int i=0; i<x; i++)//For loop traverses the array.
    {
        if (i==0)
        {
            seq[i]= 0;//assigns initial value in array to integer x passed as an argument.

        }else{
            seq[i]= seq[i-1]+1; // Generates the rest of the sequence by iterating the previous element of the array.

        }

    }

}