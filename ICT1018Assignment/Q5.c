
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>

typedef struct node
{
    int data;
    struct node* prev;
}Node;

typedef struct stack
{
    Node* head;
}Stack;

Stack* initialiseStack();
void pushToStack(int data, Stack* stac);
void popFromStack(Stack* stac);
int strngInt(char* arr, int no);
double compRPN (char* arr, Stack* stac);
void printStack(Stack* stac);

int main() {

    Stack* stac = initialiseStack();
char userInput[100];//Max length of RPN expression is 100 characters.


printf("Enter an expression to be computed in RPN format (Delimeter = Space, entire formula should also end with a space).\n");//SPACE at the end is essential for proper function!!!!
scanf("%[^\n]s",userInput);

printf("Final Result: %f\n", compRPN(userInput,stac));

}

Stack* initialiseStack()
{
    Stack* temp;
    temp = malloc(sizeof(Stack));
    temp->head = NULL;
}

void pushToStack(int data, Stack* stac)
{
    Node* temp;
    temp = malloc(sizeof(Node));//Allocating memory for node.

    if (!temp)//checking to ensure memory for node allocated
    {
        printf("Not able to push element to stack.");
        return;
    }

    if(stac->head == NULL) {//CASE for when the stack is empty
        temp->data = data;
        temp->prev=NULL;
        stac->head=temp;
    }
    else{//CASE for stack not empty
        temp->data=data;
        temp->prev=stac->head;
        stac->head=temp;
    }
}

void popFromStack(Stack* stac)
{
    Node* ptr;
    if (stac->head== NULL)//Checking if stack is empty
    {
        printf("No elements in the stack can't perform POP.");
        return;
    }
    else
    {
        ptr = stac->head;
        stac->head=stac->head->prev;
        free(ptr);
    }


}

void printStack(Stack* stac)
{
    Node *nextNode=NULL;
    nextNode=stac->head;

    printf("Current Contents of Stack: \n");

    while(nextNode != NULL)
    {
        printf("%d\n",nextNode->data);
        nextNode=nextNode->prev;
    }
    printf("\n");
}

double compRPN (char* arr, Stack* stac)
{
    int no=0;

    for (int i =0; i<strlen(arr); i++)
    {
        //If statements use space as delimiter and depend on chars behind the last space and current space.
        if(arr[i] == 32 && i!=0 && arr[i-1] != '+' && arr[i-1] != '-' && arr[i-1] != 'x' && arr[i-1] != '/' )
        {
            pushToStack(strngInt(&arr[(i-no)],no),stac);
            printStack(stac);
            no=0;
        }//looks for integers to be pushed to stack


        if(arr[i]== 32 && arr[i-1] == '+')
        {
            int ans =0;
            ans = (stac->head->data)+(stac->head->prev->data);

            for(int i=0; i<2;i++)
            {
                popFromStack(stac);
            }

            pushToStack(ans,stac);
            printStack(stac);

            no =0;
        }//takes care of addition

        if(arr[i]== 32 && arr[i-1] == '-')
        {
            int ans =0;
            ans = (stac->head->prev->data) - (stac->head->data);

            for(int i=0; i<2;i++)
            {
                popFromStack(stac);
            }

            pushToStack(ans,stac);
            printStack(stac);

            no =0;
        }//takes care of subtraction

        if(arr[i]== 32 && arr[i-1] == 'x')
        {
            int ans =0;
            ans = (stac->head->prev->data) * (stac->head->data);

            for(int i=0; i<2;i++)
            {
                popFromStack(stac);
            }

            pushToStack(ans,stac);
            printStack(stac);

            no =0;
        }//takes care of multiplication

        if(arr[i]== 32 && arr[i-1] == '/')
        {
            int ans =0;
            ans = (stac->head->prev->data) / (stac->head->data);

            for(int i=0; i<2;i++)
            {
                popFromStack(stac);
            }

            pushToStack(ans,stac);
            printStack(stac);

            no =0;
        }

      no++;
    }//takes care of division.
    return stac->head->data;
}

int strngInt(char* arr, int no)
{
    char newArr[no];
    for (int i=0; i< (no);i++)
    {
        newArr[i]=arr[i];
    }

    return atoi(newArr);
} //Converts strings to ints