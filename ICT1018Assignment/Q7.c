//
// Created by benja on 4/7/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>
#define  SEP 10

typedef struct node
{
    int data;
    struct node *right;
    struct node *left;
}Node;

Node *insert(int d,Node *root);
Node *newNode(Node *root, int d, bool side);
bool searchArr(int *arr, int i,int d);
void display(Node *root, int space);

int main()
{
  Node *root = NULL;//Initialising root node

  bool replicate = false; //
  int n;
  printf("How many numbers do you wish to enter? \n");
  scanf("%d", &n);

  int arr[n];//used for input validation as to avoid duplicate nodes

  for (int i=0;i<n;i++)
  {
      int d=0;
      printf("Input the next integer. \n");
      scanf("%d", &d);
      replicate= searchArr(arr,i,d);

      while (replicate == false) { //Keeps looping until user enters number not previously entered.
          printf("Number already entered, chose a different one!");
          scanf("%d", &d);
          replicate= searchArr(arr,i,d);
      }
      replicate =false;//Resets replicate to false.

      arr[i]=d;

      if(i==0)
      {
          root=newNode(NULL,d,false);
          display(root,0);
          continue;
      }else{
          insert(d,root);
          display(root,0);
      }
  }

}

Node *insert(int d, Node *root)//Basically another newNode function dealing directly with nodes inserted after the root.
{
    bool side;
    Node *nextNode=root;
    Node *prevNode=root;
  while(nextNode != NULL)
  {
      if(d < nextNode->data)
      {
          side = true;
          prevNode=nextNode;
          nextNode = nextNode->left;
      }else if(d > nextNode->data)
      {
          side =false;
          prevNode=nextNode;
          nextNode = nextNode->right;
      }
  }

newNode(prevNode,d,side);

}

Node *newNode(Node *root, int d, bool side)
{
    if(root==NULL)//inserting the root node
    {
        Node *node = (Node*)malloc(sizeof(Node));
        node->data=d;
        node->left =NULL;
        node->right =NULL;
        return(node);
    }
    if(side == true)//Inserting on the left
    {
        root->left = (Node*)malloc(sizeof(Node));
        root->left->data=d;
        root->left->left =NULL;
        root->left->right=NULL;
        return (root->left);
    }else{ //Inserting on the right
        root->right =(Node*)malloc(sizeof(Node));
        root->right->data=d;
        root->right->left=NULL;
        root->right->right=NULL;
        return (root->right);
    }
}

bool searchArr(int *arr, int i,int d)
{
    if (i==0)
    {
        return true;
    }
    for(int j=0;j<i;j++)
    {
        if(arr[j]==d)
            return false;
    }
    return true;
}

void display(Node *root, int space)
{
    if(root == NULL)
        return;

    space += SEP;

    display(root->right,space);

    printf("\n");

    for (int i = SEP; i<space;i++)
    printf(" ");

    printf("%d\n", root->data);

    display(root->left, space);
}