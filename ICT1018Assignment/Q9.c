//
// Created by benja on 4/9/2020.
//

#include <stdio.h>
#include <stdbool.h>
#define SEQ_LEN 8

void findDup(int *seq);

int main()
{
    int Sequence[SEQ_LEN]={10,45,32,5,10,32,32,5};

    findDup(Sequence);
}

void findDup(int *seq)
{
    bool hashMap[SEQ_LEN]={false};
    for (int i=0;i<SEQ_LEN;i++)
    {
        for (int j = i+1; j < SEQ_LEN; j++)
        {
            if(i== SEQ_LEN-1)
            {
                break;
            }
            if(hashMap[i]==true ||hashMap[j]==true)
                continue;
            if(seq[i]+seq[j]== 2*seq[i])
            {
                printf("Repeated Element Found: %d\n", seq[i]);
               hashMap[j]=true;
               hashMap[i]=true;
            }
        }
    }
}