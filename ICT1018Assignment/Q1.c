//
// Created by benja on 3/1/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

#define SHELL_LEN 15 //Length of array used for Shell Sort
#define QUICK_LEN 20 //Length of array used for Quick Sort

void ShellSort(int* arr, int arrSize);
void quickSort(int arr[], int low,int high);
void generate(int *seq, int length);

int main()
{
int arrA[SHELL_LEN] ;
int arrB[QUICK_LEN];

generate(arrA,SHELL_LEN);
generate(arrB,QUICK_LEN);

quickSort(arrB, 0,QUICK_LEN-1);

    printf("Quick Sort on Array B: "); //Contents of sorted array are printed on screen
    for (int i=0;i<QUICK_LEN;i++)
    {
        printf("%d ",arrB[i]);
    }
    printf("\n");
ShellSort(arrA,SHELL_LEN);

}

void ShellSort(int* arr, int arrSize)
{
    for (int gap = arrSize/2; gap>0; gap/=2) //Sets gap to (size of the array)/2 initially, then divides gap by 2 at each iteration until gap=0;
    {
        for (int i = gap; i<arrSize; i++)//For loop for comparing the elements in the array seperated by the gap sepcified above.
        {
            int temp = arr[i];//Variable for temporarily storing the element at position [i].
            int j; //variable for below for loop initialised outside since it is re used outside of the for loop.

            for (j=i; j>= gap && arr[j-gap]>temp;j-=gap)
                /*For loop that acts more like a for loop and an if condition combined into one.
 *                Checks to see if the left most element being compared ie element @ [j-gap] is greater than that @ [i] (or variable temp)
                 *If so the elements are swapped.              */
            {
                arr[j] = arr[j-gap];
            }
            arr[j] = temp;
        }
    }

    printf("Shell Sort on Array A: "); //Contents of sorted array are printed on screen
    for (int i=0;i<SHELL_LEN;i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
}

void swap(int* x, int* y)
{
    int t = *x;
    *x = *y;
    *y = t;
}

int partition(int arr[], int low, int high)
{
    int pivot = arr[high];
    int i = (low -1);

    for (int j = low; j<=(high-1);j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap (&arr[i+1], &arr[high]);
    return (i+1);
}

void quickSort(int arr[], int low,int high) {

    if (low < high) {

        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }

}

void generate(int *seq, int length)
{

    for (int i=0; i<length; i++)//For loop traverses the array.
    {
            seq[i]= (rand()%1025);//assigns random number between 0-1024 to all entries in the array.
    }

}