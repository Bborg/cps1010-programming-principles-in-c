//
// Created by benja on 3/2/2020.
//

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <math.h>

int factorial(int c);
double cosineExp(int n,double x);

int main ()
{
printf("%f",cosineExp(10, 160) );
}

double cosineExp(int n,double x) //n is the number of iterations,
{
    double ans= 0;

    x= x*(3.1459/180); // Conversion for degrees approximating pi to 3.1459

    for (int i=0;i<n;i++)//For loop for iteratively approximating the value of cosine using its macluarins series expansion
    {
        ans += (pow(-1,i)*pow(x,2*i))/(factorial(2*i));
    }

    return ans;
}

int factorial(int c) //function for computing factorial.
{
    double ans=1;

    for( int i=c;i>1;i--)
    {
        ans *= i;
    }

    return ans;
}