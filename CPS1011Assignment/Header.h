//
// Created by benja on 11/12/2019.
//

#ifndef CPS1011ASSIGNMENT_HEADER_H
#define CPS1011ASSIGNMENT_HEADER_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define LEN 5 // Specifies size of global array "sequence".
#define LEN1 10
#define LEN2 6

int total ;   // Global variable used in func target()
int total1;

//Funcs for 1(a).
void generate(int *seq);// Func that populates an array with a sequence of ascending integers starting from i, i>0.
void shuffle(int *seq);// Func that takes a populated array as an input and shuffles the entities in the array randomly.
void sort(int *seq);//Func that takes a populated array as an input and sorts it using the bubble sort algorithm.
bool shoot(int *seq,bool x);//Fun that zeroes out a random element from an array.
int target(int *seq);//returns the value of the element zeroed out by shoot.

//Funcs for 1(b).
void generateS(char (*seq)[LEN2], int *s);
void shuffleS(char (*seq)[LEN2],int *s);
void sortS(char (*seq)[LEN2],int *s);
bool shootS(char (*seq)[LEN2],int *s, bool x);
char* targetS(char (*seq)[LEN2],int *s);
#endif //CPS1011ASSIGNMENT_HEADER_H
