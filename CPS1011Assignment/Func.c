//
// Created by benja on 11/12/2019.
//

#include "Header.h"



void generate(int *seq)
{

    for (int i=0; i<LEN; i++)//For loop traverses the array.
    {
        if (i==0)
        {
            seq[i]= (rand()%50 +1);// Randomly assigns the start of the sequence to a number between 1 and 50.

        }else{
            seq[i]= seq[i-1]+1; // Generates the rest of the sequence by iterating the previous element of the array.

        }

    }

}

void sort(int *seq)//sorts an array passed by a pointer, using a bubble sort algorithm.
{
    for (int j =0; j<LEN;++j) {

        for (int i = 0; i < LEN; ++i)
        {
            if (i != (LEN - 1) && seq[i] > seq[i + 1])/*Checks that index isn't pointing to last element and
 *                                                       checks if current element is greater than the next.*/

            {
                int x = seq[i], y = seq[i + 1];

                seq[i] = y;
                seq[i + 1] = x;
            } else{
                continue;
            }

        }
    }
}

void shuffle(int *seq)
{
    for (int i=0; i<LEN;++i)
    {
        if (i== (LEN-1))
        {
            break;
        }else {
            int x = seq[i];
            int y = rand() % (LEN - (i + 1)) + (i + 1);

            seq[i] = seq[y];
            seq[y] = x;
        }
    }
}

bool shoot(int *seq,bool x)
{
    for (int i=0;i<LEN;i++)
    {
        total += seq[i];
    }
  if (x==true)
  {
      printf("Function already called!\n");
     return true;
  }else{
      int y =rand()%LEN;
      seq[y]=0;
      printf("\nEntity at array index %d, set to 0.\n", y);

      return true;
  }
}

int target(int *seq)
{
    for (int i=0;i<LEN;i++)
    {
        total -= seq[i];

    }
    return total;
}

char* trans(int no)
{
    char* ret=malloc(LEN2);

    switch(no)
    {
        case 1:

            return "one\0" ;
        case 2:
            ret ="two\0";
            return ret;
        case 3:
            ret = "three\0";
            return ret;
        case 4:
            ret = "four\0";
            return ret;
        case 5:
            ret = "five\0";
            return ret;
        case 6:
            ret = "six\0";
            return ret;
        case 7:
            ret = "seven\0";
            return ret;
        case 8:
            ret = "eight\0";
            return ret;
        case 9:
            ret = "nine\0";
            return ret;
        case 10:
            ret = "ten\0";
            return ret;


    }

}

void generateS(char (*seq)[LEN2],int *s)
{
    for (int i =0;i<LEN1;++i)
    {
        s[i]= i+1;
       char *c = trans(i+1);

       strcpy(seq[i], c);

    }
}

void shuffleS(char (*seq)[LEN2],int *s)
{
    for (int i=0; i<LEN1;++i)
    {
        if (i== (LEN1-1))
        {
            break;
        }else {
            int x = s[i];
            int y = rand() % (LEN1 - (i + 1)) + (i + 1);

            s[i] = s[y];
            s[y] = x;
        }
    }
    for (int i=0; i<LEN1;i++)
    {
        strcpy(seq[i],trans(s[i]));
    }
}

void sortS(char (*seq)[LEN2],int *s)
{
    for (int j =0; j<LEN1;++j) {

        for (int i = 0; i < LEN1; ++i)
        {
            if (i != (LEN1 - 1) && s[i] > s[i + 1])/*Checks that index isn't pointing to last element and
 *                                                       checks if current element is greater than the next.*/

            {
                int x = s[i], y = s[i + 1];

                s[i] = y;
                s[i + 1] = x;
            } else{
                continue;
            }
            for (int k=0; k<LEN1;k++)
            {
                strcpy(seq[k],trans(s[k]));
            }

        }
    }
}

bool shootS(char (*seq)[LEN2],int *s, bool x)
{
    for (int i=0;i<LEN;i++)
    {
        total1 += s[i];
    }
    if (x==true)
    {
        printf("Function already called!\n");
        return true;
    }else{
        int y =rand()%LEN;
        s[y]=0;
        printf("\nEntity at array index %d, set to 0.\n", y);

        for (int i=0; i<LEN1;i++)
        {
            if (s[i]==0)
            {
                strcpy(seq[i], "zero\0");
                break;
            }

        }

        return true;
    }

}

char* targetS(char (*seq)[LEN2],int *s)
{
    for (int i=0;i<LEN;i++)
    {
        total1 -= s[i];

    }
    return trans(total1);
}