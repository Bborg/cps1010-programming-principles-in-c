#include "header.h"

int main(){

    int choiceMain, idUser;
    MsgQs_t *pointer;
    Item itemUser;

    do {
        Menu();
        scanf("%d", &choiceMain);


        switch (choiceMain) {
            case 1:
                pointer = intializeMsgQs();
                break;
            case 2:
                pointer = unloadMsgQs(pointer);
                break;
            case 3:
                idUser = askId(idUser);

                createQ(pointer, idUser);
                break;
            case 4:
                ListQs(pointer);
                break;
            case 5:
                idUser = askId(idUser);

                deleteQ(pointer, idUser);
                break;
            case 6:
                sendMessage(pointer);
                break;
            case 7:
                sendMessageBatch(pointer);
                break;
            case 8:
                receiveMessages(pointer);
                break;
            case 9:
                purgeQs(pointer);
                break;
            case 10:
                persistQ(pointer);
                break;
            case 11:
                restoreQ(pointer);
                break;
            case 12:
                exit(EXIT_SUCCESS);
            default:
                printf("Please enter a valid option\n");
                break;
        }

    }while(choiceMain != 12);


    return 0;
}
