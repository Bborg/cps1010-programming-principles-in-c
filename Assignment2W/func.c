//
// Created by benja on 12/30/2019.
//

#include "header.h"

//Local Functions prototypes
static void CopyToNode(Item item, Node * pnode);


MsgQs_t *intializeMsgQs(){
    MsgQs_t *ptr = (MsgQs_t *) malloc(sizeof (MsgQs_t)); // allocating space for the type

    if(ptr == NULL){ //Error check to make sure memory is allocated
        printf("Could not allocate Memory space!");
        return NULL;
    }

    ptr->qNo = 0; //setting the queue count to 0;

    printf("\nIntialisation Completed!\n");

    return ptr;
}

MsgQs_t *unloadMsgQs(MsgQs_t *pt){ // Frees the space previously allocated by intializeMsgQs()
    free(pt);                      //Returns a NULL
    pt = NULL;

    printf("\nUnloading Complete!\n");
    return pt;
}

bool CheckUniqueId(MsgQs_t *pt, int id){ //Checking if the queue id entered is unique
    for(int i=0; i<MAXQUEUE; ++i){
        if(pt->queues[i].id == id) // comparing user entered id with already existing id
            return false;
    }
    return true;  // If it is unique the function returns true.
}

void createQ(MsgQs_t *pt, int id){

    if(CheckUniqueId(pt, id) == false){ //Id is not unique
        printf("The Id entered is not unique.");
        return;
    }
    if(pt->qNo < MAXQUEUE) {
//Create respective queue
        pt->queues[pt->qNo].id = id;
        pt->queues[pt->qNo].front = pt->queues[pt->qNo].rear = NULL; //Setting front and rear pointers to null
        pt->queues[pt->qNo].size = 0; //No messages

        (pt->qNo)++; //Updating queue number

    }else{
        printf("Max queues created! Cannot create more queues\n");
        return;
    }
}

void ListQs(MsgQs_t *pt){

    for(int i=0; i<pt->qNo; ++i){
        printf("Queue Id: %d\n", pt->queues[i].id);
        Traverse(pt,pt->queues[i].id);
        printf("\n");
    }
}

void deleteQ(MsgQs_t *pt, int id){

    int queueIndex;

    if(CheckUniqueId(pt,id) == true){ //id is unique
        printf("This id does not exist!");
        return;
    }

    queueIndex = searchQueueID(pt, id);

    if(queueIndex == (pt->qNo -1) ){//It is the last queue
        free(&pt->queues[queueIndex]);
        pt->qNo--;
    }else{
        free(&pt->queues[queueIndex]);
        pt->qNo--;
        for(int i=0; i<pt->qNo; ++i){ //Form the deleted queue to the end of occupied queues
            pt->queues[queueIndex+i] = pt->queues[queueIndex+i+1]; // To remove the gap space
        }
    }
}

bool QueueIsEmpty(MsgQs_t *pt, int index){
    if(pt->queues[index].size == 0) //Checks for size, if size is 0 means queue is empty
        return true;

    return false;
}

int searchQueueID(MsgQs_t *pt, int id){

    for(int i=0; i<MAXQUEUE; ++i){
        if(pt->queues[i].id == id){
            return i; //returning queue number/index
        }
    }

    return -1; //id not found
}

static void CopyToNode(Item item, Node * pnode) { //Function is part of the implementation not interface
    pnode->item = item;                          //Static storage class qualifier is used to hide it
}

bool QueueIsFull(MsgQs_t *pt, int index){
    if(pt->queues[index].size == MAXQUEUE) //Will look for specific queue and check size
        return true;

    return false;
}

void enQueue(MsgQs_t *pt, Item item, int index){

    if(QueueIsFull(pt, index) == true) { //Checking if the queue specified is full
        printf("Queue ID: %d is full", pt->queues[index].id);
        return;
    }

    Node *pnew = (Node *) malloc(sizeof(Node)); //Allocating space for node


    if(pnew == NULL){ //Making sure memory allocation was successful
        fprintf(stderr, "Unable to allocate memory!\n");
        exit(1);
    }


    CopyToNode(item, pnew); //Attaching message(item) to the node
    pnew->next = NULL; //The node is the last in the list

    if(QueueIsEmpty(pt, index) == true) {
        pt->queues[index].front = pt->queues[index].rear = pnew; //Item goes to the front.
        //Since there is only one item both front and rear must point to it
        pt->queues[index].size++; //Incrementing size of specific queue
    }else{ // If queue is not full
        pt->queues[index].rear->next = pnew; //Link at the end of queue
        pt->queues[index].rear = pnew; //record location at the end
        pt->queues[index].size++; // incrementing size of the specific queue
    }
}

void sendMessage(MsgQs_t *pt){
    int id;
    Item item;

    printf("To which queue would you like to send the message to:\n");
    printf("Enter -1 to send to all queues!\n");
    scanf("%d", &id);
    fflush(stdin);



    if(id == -1){
        printf("Enter the message to send to all the queues\n");
        fgets(item.msg, MAXLENGTH, stdin); // reading input & setting null terminator (as per def of fgets)

        for(int i= 0; i<pt->qNo; ++i){
            enQueue(pt,item, i); //Adding the message to all queues through enQueue() function
        } //Note how index is passed as id

    }else if(searchQueueID(pt, id) != -1){ //id is found
        printf("Enter message you want to send to Queue Id: %d\n", id);
        fgets(item.msg, MAXLENGTH, stdin); //reading input & setting null terminator (as per def of fgets)
        enQueue(pt,item,searchQueueID(pt,id)); //SearchCall is used to pass index

    }else{
        printf("Error id not found!\n"); //Displaying error message
    }
}

void sendMessageBatch(MsgQs_t *pt){

    int amount;
    printf("How many messages do you wish to send: ");
    scanf("%d", &amount);

    Item item[amount]; //Creating the array of messages to be sent at once
    int id;

    printf("\n\nTo which Queue would you like to send the messages: \n");
    printf("Enter -1 to send to all created queues\n");
    scanf("%d", &id);

    fflush(stdin);

    if(id == -1){
        for(int i=0; i<amount; ++i){
            printf("\nEnter message no: %d to be sent to all queues\n", i+1);
            fgets(item[i].msg, MAXLENGTH, stdin); //Gathering messages to be sent to all queues
        }
        //Sending the messages
        for(int i=0; i<amount; ++i){ //For all items
            for(int j=0; j<pt->qNo; ++j){ //For each item & for all queues
                enQueue(pt,item[i],j); //send message
            }
        }

    }else if(searchQueueID(pt, id) != -1){ //id for queue is found
        for(int i=0; i<amount; ++i){
            printf("Enter message no: %d to be sent to queue id: %d\n", i+1,id);
            fgets(item[i].msg, MAXLENGTH, stdin); //Gathering the messages
            enQueue(pt,item[i],searchQueueID(pt,id)); //Adding them to queue
        }
    }else{
        printf("Id not found!\n");
        printf("Messages not sent!\n");
    }
}

bool deQueue(MsgQs_t *pt, int id){

    int index = searchQueueID(pt,id); //retaining index of specified queue

    if(QueueIsEmpty(pt,index)){ //if true
        printf("Queue: %d is already empty!", id);
        return false;
    }

    Node *ptr; //Temporary pointer to keep track of deleted node's location
//Since pointer to the first node gets set to next node

    ptr = pt->queues[index].front; //Node points to the front
    pt->queues[index].front = pt->queues[index].front->next; //Front pointer set to the next pointer being deleted
    free(ptr); //Freeing up the previously allocated space

    pt->queues[index].size--; //Decrementing size of queue

    if(pt->queues[index].size == 0) //If queue is empty
        pt->queues[index].rear = NULL; //setting rear pointer to NULL indicates last item was removed
    //Front set to NULL automatically, since if it was the last node the next pointer would contain NULL

    return true;
}

void receiveMessages(MsgQs_t *pt){

    int noToRead, id;


    printf("Enter the Queue id you wish to read the messages from: \n");
    scanf("%d", &id);

    int index = searchQueueID(pt,id);

    if(index == -1){//Searching for queue
        printf("Queue id does not exist! \n");
        return;
    }

    printf("Size of selected queue is: %d\n", pt->queues[index].size);//Displaying size

    printf("Enter the number of messages you wish to read: \n");
    scanf("%d", &noToRead);

    if(pt->queues[index].size<noToRead){//Checking amount to be read is valid
        printf("The number of messages you trying to read is greater than the amount in queue.\n");
        return;
    }

    for(int i=0; i<noToRead; ++i){//removing the messages read
        deQueue(pt,id); //deQueue function converts id to index within it
    }
}

void purgeQs(MsgQs_t *pt) {
    int id, queueSize;

    printf("Enter the ID of the queue u wish to purge\n");
    printf("Enter -1 to purge all queues\n");
    scanf("%d", &id);

    if ((id == -1) && (pt->qNo>0)){ //Purging all queues, provided that at least a queue exists

        for(int i=0; i<pt->qNo; ++i){//For all queues
            queueSize = pt->queues[i].size; //Retaining size of queue
            for(int j=0; j<queueSize; ++j){//For all of the specific queue
                deQueue(pt,pt->queues[i].id);
            }
        }
    }else if((id != -1) && (pt->qNo>0)){//If a queue id is specified & queues do exists
        if(searchQueueID(pt,id) == -1){//Checking if queue does exist
            printf("Queue id does not exist");
            return;
        }else{//If it does
            for(int i=0; i<pt->queues[searchQueueID(pt,id)].size; ++i){//For all the of the size of the queue
                deQueue(pt,id);
            }
            return;
        }
    }else{
        printf("No queues created to be purged");
    }
}

void Traverse(MsgQs_t *pt, int id){
    //pfun is a pointer to a function
    Node *pnode = pt->queues[searchQueueID(pt,id)].front; //Set to start of queue

    while(pnode != NULL){//Until end of queue is reached
        printf("\tMessage: %s",pnode->item.msg); //Display the next
        pnode = pnode->next; //Move to the next pointed
    }
}

void persistQ(MsgQs_t *pt){
    int id, queueIndex;

    printf("Enter the queue you wish to search for: ");
    scanf("%d", &id);

    queueIndex = searchQueueID(pt,id);//retaining index

    if(queueIndex == -1) {//Checking for id
        printf("Error ID not found!\n");
        return;
    }


    char array[MAXQUEUE*MAXLENGTH] = {'\0'};//Creating array to store contents of queue
    int arraySize = 0;

    while(pt->queues[queueIndex].front != NULL){
        strcpy(&array[arraySize], pt->queues[queueIndex].front->item.msg);//Storing contents of queue to array
        pt->queues[queueIndex].front = pt->queues[queueIndex].front->next; //traversing to next link in circular queue
        arraySize = (int) strlen(array);//Getting the current array size
    }

    char fileName[20];
    char prefix[] = {"queue_"};
    char postfix[]= {".dat"};
    char QueueId[10]= {'\0'};

    itoa(pt->queues[queueIndex].id, QueueId, 10);
    snprintf(fileName, 20, "%s%s%s", prefix, QueueId, postfix);//Combining filename

    FILE *ptr;
    ptr = fopen(fileName, "wb");//opening file

    if(ptr == NULL){//Checking file opened
        printf("Unable to open a file.");
        exit(EXIT_FAILURE);
    }

    fwrite(array, 1, sizeof(array), ptr);//writing to file
    fclose(ptr);//closing file

    deleteQ(pt,id);//removing queue that was stored into file


    printf("File was created and contents were stored\n");
}

void restoreQ(MsgQs_t *pt) {
    char fileName[20];
    int id, i = 0;
    char *ptr;


    printf("Enter the file name that you want restored.\n");
    printf("Please abide by format of queue_id.dat\n");
    scanf("%s", fileName);

    FILE *fp = fopen(fileName, "r");

    if (fp == NULL) {
        printf("The filename entered does not exist.\n");
        return;
    }

    ptr = fileName;
    while (*ptr) {
        if (isdigit(*ptr)) {
            id = (int) strtol(ptr, &ptr, 10);
        } else {
            ptr++;
        }
    }

    if (!CheckUniqueId(pt, id)) {
        printf("ID already exists!\n");
        fclose(fp);
        return;
    }

    createQ(pt, id);

    long fileSize = GetFileSize(fp);
    if (fileSize == -1) {
        fclose(fp); // File would be open by now so we close
        printf("Error in retrieving file size\n");
        return;
    }

    char *inputString = malloc(fileSize); // Allocating memory
    if (inputString == NULL) {
        printf("malloc() fail!\n");
        fclose(fp);
        return;
    }

    rewind(fp); //goes to the beginning of the file

    size_t numElementsRead = fread(inputString, 1, fileSize, fp);//reads to inputString all elements in the file.
    assert(numElementsRead == fileSize); // to make sure all bytes in file are read

    fclose(fp);

    //Writing individual strings to respective nodes.
    char *offset = inputString;
    char ch;
    int count=0;

    for(int i=0; i<strlen(inputString); ++i){
        ch = inputString[i];
        if(ch == '\n'){
            count ++;
        }
    }

    Item item[count];
    int j=0;

    for(;;){
        char *found = strstr(offset, "\n");
        if(found == NULL){
            break;
        }
        size_t bytesToWrite = (size_t)(found - offset);//offset between string found and start of search.
        memcpy(item[j].msg, offset, bytesToWrite+1);
        enQueue(pt,item[j],searchQueueID(pt,id));
        offset += bytesToWrite+1;
        j++;
    }

    printf("Queue restored!\n");
    free(inputString);
}

long GetFileSize(FILE *f){
    int ret = fseek(f, 0L, SEEK_END); //going to end of file

    if(ret != 0){
        printf("fseek() failed with error %d\n", errno);
        return -1;
    }

    long fileSize = ftell(f); //Getting size of file
    if(fileSize == -1){
        printf("ftell() failed with error %d\n", errno);
        return -1;
    }

    return fileSize;
}

void Menu(void){
    printf("\n\n******MESSAGE QUEUE MENU******\n*%29s\n", "*");
    printf("*1.    intialiseMsgQs   ******\n");
    printf("*2.    unloadMsgQs      ******\n");
    printf("*3.    createQ          ******\n");
    printf("*4.    listQs           ******\n");
    printf("*5.    deleteQ          ******\n");
    printf("*6.    sendMessage      ******\n");
    printf("*7.    sendMessageBatch ******\n");
    printf("*8.    receiveMessages  ******\n");
    printf("*9     purgeQs          ******\n");
    printf("*10    persistQ         ******\n");
    printf("*11    restoreQ         ******\n");
    printf("*12    exit             ******\n");
    printf("******************************\n");
    printf("\nPlease enter your choice: ");

}

int askId(int id){
    printf("Please enter id: ");
    scanf("%d", &id);

    return id;
}
