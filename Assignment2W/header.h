//
// Created by benja on 12/30/2019.
//

#ifndef ASSIGNMENT2W_HEADER_H
#define ASSIGNMENT2W_HEADER_H
#include <stdio.h>
#include <stdlib.h> // for malloc() & free()
#include <stdbool.h> // for bool type
#include <string.h> // for strlen()
#include <ctype.h>
#include "assert.h"
#define MAXLENGTH 40 //Max length of message (including null terminator)
#define MAXQUEUE 10 //Max amount of queues

typedef struct message{//Creating Item
    bool read;
    char msg[MAXLENGTH];
}Item;

typedef struct node{ //Creating type Node
    Item item;
    struct node * next; // a pointer to point to the next item
}Node;

typedef struct circularQueue{ //Creating type cQueue
    int id; // unique identifier for the queue
    int size; // The number of messages in the queue
    Node * front; //pointer to the front of the queue
    Node * rear; // pointer to the back of the queue
} cQueue;

typedef struct msgQs_t{ //creating type MsgQs_t
    cQueue queues[MAXQUEUE]; //An array of cQueues
    int  qNo; //Number of queues
}MsgQs_t;

//Prototypes
MsgQs_t *intializeMsgQs();
MsgQs_t *unloadMsgQs(MsgQs_t *pt);
bool CheckUniqueId(MsgQs_t *pt, int id);
void createQ(MsgQs_t *pt, int id);
void ListQs(MsgQs_t *pt);
void deleteQ(MsgQs_t *pt, int id);
bool QueueIsEmpty(MsgQs_t *pt, int index);
bool QueueIsFull(MsgQs_t *pt, int index);
int searchQueueID(MsgQs_t *pt,int id);
void enQueue(MsgQs_t *pt, Item item, int index);
void sendMessage(MsgQs_t *pt);
void sendMessageBatch(MsgQs_t *pt);
bool deQueue(MsgQs_t *pt, int id);
void receiveMessages(MsgQs_t *pt);
void purgeQs(MsgQs_t *pt);
void persistQ(MsgQs_t *pt);
void restoreQ(MsgQs_t *pt);
void Traverse(MsgQs_t *pt, int id);
void Menu(void);
int askId(int id);
long GetFileSize(FILE *f);
#endif //ASSIGNMENT2W_HEADER_H
