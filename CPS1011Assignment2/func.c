#include "header.h"

MsgQs_t* initialiseMsgQs()
{
    MsgQs_t* ptr = malloc(sizeof (MsgQs_t)); //Allocating space for msgQueues
    if (ptr == NULL)
        return NULL;
    ptr->noQ=0;


    return ptr;
}

void createQ(MsgQs_t *new)
{
    int id;

    if(new->noQ == MAX_QUEUES)
    {
        printf("Queues full . \n");
        return;
    }

    for (;;) {//loops until user enters valid ID

        printf("Enter id for new queue, ID must be in form XX where X is a digit and XX is an integer > 10.\n");
        scanf("%d", &id);
        if (id < 10) {
            printf("Error ID invalid!.");
            continue;
        }
       break;
    }

    if (checkIDUnique(new)==false)
    {
        printf("Error ID not Unique!!");
        return;
    }

    new->Q[new->noQ].idNo=id;
    new->Q[new->noQ].front=new->Q[new->noQ].rear=NULL;
    new->Q[new->noQ].size=0;
    new->Q[new->noQ].cSize=0;

    (new->noQ)++;
}

bool checkIDUnique(MsgQs_t *Q)
{

    for (int i=0;i<Q->noQ;i++)
    {
        if (Q->Q[i].idNo == Q->noQ)
        {
            return false;
        }
    }

    return true;
}

MsgQs_t* unloadMsgQs(MsgQs_t *Q)
{
    free(Q);
    Q = NULL;
    return Q;
}

void listQs(MsgQs_t *Q)
{
   sortQsID(Q);

    for (int i = 0; i <Q->noQ;i++)
    {
        printf("Queue ID: %d, Index : %d\n",Q->Q[i].idNo, i);
        traverseDisp(Q,Q->Q[i].idNo);
    }

}

void sortQsID(MsgQs_t *Q)
{
    Queue x;
    Queue y;
    bool end = true;

    do {
        end = true;
        for (int j = 0; j < Q->noQ; j++) {
            if ((j != (Q->noQ - 1)) && (Q->Q[j].idNo > Q->Q[j + 1].idNo)) {
                end = false;
                x = Q->Q[j];
                y = Q->Q[j + 1];

                Q->Q[j] = y;
                Q->Q[j + 1] = x;

            } else {
                continue;
            }
        }
    }while(end == false);
}

void deleteQ(MsgQs_t *delete,int id)
{
    int index;
    if(searchQs(delete,id)==-1)
    {
        printf("Could not find queue to be deleted!\n");
        return;
    }else{
        index = searchQs(delete, id);
    }
    if(index==(delete->noQ-1))
    {
        free(&delete->Q[index]);
        delete->noQ--;
    }else{
        free(&delete->Q[index]);
        delete->noQ--;
        for (int i=0;i<delete->noQ;i++)
        {
            delete->Q[index+i]=delete->Q[index+i+1];
        }
    }

}

void sendMessage(MsgQs_t *Q)
{   int id;
    Item new;

    printf("Which Queues do you wish to send to: \n");
    printf("Enter -1 to send to all Queues. \n");
    (scanf("%d", &id));
    fflush(stdin);
    if (id == -1)
    {
        printf("Enter message to be sent to all queues. \n");
        fgets(new.msg,MAX_MSG_LEN,stdin);
        fflush(stdin);
        new.msg[strlen(new.msg)+1]= '\0';//inserting the null term.
        for(int i =0;i<Q->noQ;i++)
        {
            enQueue(Q,new,i);
        }
        return;
    }else if (searchQs(Q,id)!=-1){
        printf("Enter message to be sent to Queue ID: %d. \n",id);
        fgets(new.msg,MAX_MSG_LEN,stdin);
        new.msg[strlen(new.msg)+1]= '\0'; //inserting the null term.
        enQueue(Q,new,searchQs(Q,id));
        return;
    }else{
        printf("Error ID not Found\n");
        return;
    }
}

void sendMessageBatch(MsgQs_t *Q)
{
    int no;
    printf("How many messages do you wish to send out.\n");
    scanf("%d",&no);
    fflush(stdin);

    int id;
    Item new[no];

    printf("Which Queues do you wish to send to: \n");
    printf("Press -1 to send to all Queues. \n");
    scanf("%d",&id);
    fflush(stdin);
    if ( id == -1)
    {
        for (int i=0; i<no;i++) {
            printf("Enter message no: %d, to be sent to all queues. \n",i+1);
            fgets(new[i].msg,MAX_MSG_LEN,stdin);
            new[i].msg[strlen(new[i].msg)+1]='\0';//inserting the null term

        }

        for (int j=0;j<no;++j) {
            for (int i = 0; i < Q->noQ; i++) {
                enQueue(Q, new[j], i);
            }
        }
    }else if(searchQs(Q,id) != -1){
        for (int i =0;i<no;i++) {
            printf("Enter message no:%d, to be sent to  Queue ID: %d. \n", i + 1, id);
            fgets(new[i].msg,MAX_MSG_LEN,stdin);
            new[i].msg[strlen(new[i].msg)+1]='\0';//inserting teh null term
            enQueue(Q,new[i],searchQs(Q,id));
        }

    }else{
        printf("Error ID not found!\n");
    }
}

void receiveMessages(MsgQs_t *Q)
{
    int id;
    int no;

printf("Enter ID of Queue you wish to read from.\n");
scanf("%d",&id);

if (searchQs(Q,id)==-1)
{
    printf("Queue doesn't exist!!");
    return;
}

printf("Enter number of messages you wish to read off.\n");
scanf("%d",&no);

if(no>Q->Q[searchQs(Q,id)].size)
{
    printf("Number of messages to be read, greater than messages in queue.\n");
           return;
}

for(int i=0;i<no;++i)
{
    deQueue(Q,id);
}
}

void enQueue(MsgQs_t *Q, Item new,int id)
{
    if (Q->Q[id].size == MAX_MSGS)
    {
        printf("Queue ID:%d, is full!!", Q->Q[id].idNo);
        return;
    }
    Node * pnew = (Node *) malloc(sizeof(Node));
    copyToNode(new,pnew);
    pnew->next = NULL;

    if (Q->Q[id].size == 0)//queue is emtpy hence both rear and front must point to new item .
    {
        Q->Q[id].front = Q->Q[id].rear = pnew;
        Q->Q[id].size++;
        Q->Q[id].cSize += strlen(new.msg);

    }else{//if queue isn't empty traverse until end of queue is reached and point previous rear element to the new rear element.
        Q->Q[id].rear->next = pnew;
        Q->Q[id].rear = pnew;
        Q->Q[id].size++;
        Q->Q[id].cSize += strlen(new.msg);
    }
}

void deQueue(MsgQs_t *Q,int id)
{
    int index=searchQs(Q,id);
    int y;
    Node * ptr;
    if (Q->Q[index].size == 0)
    {
        printf("Queue ID:%d, is empty!!", id);
        return;
    }else
    {
        ptr = Q->Q[index].front;
        y =strlen(Q->Q[index].front->item.msg);
        Q->Q[index].front=Q->Q[index].front->next; //moving the front pointer to the next element
        Q->Q[index].size--;
        Q->Q[index].cSize -= y;
        free(ptr);

        if(Q->Q[index].size==0)
        {
            Q->Q[index].rear=NULL;
        }
    }
}

void copyToNode(Item item, Node * pnode)
{
    pnode->item=item;
}

int searchQs(MsgQs_t *Q,int id)
{
    sortQsID(Q);

    for (int i=0; i<Q->noQ;i++)
    {
        if(Q->Q[i].idNo==id)
        {
            return i;
        }
    }
    return -1;
}

void purgeQs(MsgQs_t *Q)
{int id, x;
    printf("Which queue would you like to purge?\n");
    printf("Enter -1 to purge all queues.\n");
    scanf("%d",&id);

    if((id==-1)&&(Q->noQ>0))
    {
        for(int i=0; i<Q->noQ;++i)
        {
            x = Q->Q[i].size;
            for (int j=0;j<x;j++) {
                deQueue(Q, Q->Q[i].idNo);
            }
        }
    }else if(id != -1)
    {
        int index = searchQs(Q,id);
        if (index ==-1)
        {
            printf("Queue to be purged not found!!");
            return;
        }
          x = Q->Q[index].size;
        for (int j=0;j<(x);j++) {
            deQueue(Q, Q->Q[index].idNo);
        }
        return;
    }


}

void persistQ(MsgQs_t *Q)
{
    int id;//id of queue to be pushed to file.
    int Len;//no of messages in queue.
    int initSize;//size in bytes of all messages in queue.
    char prefix[]={"queue_"};
    char postfix[]={".dat"};


    printf("Enter ID of queue you wish to push to file.\n");
    scanf("%d",&id);
    fflush(stdin);

    if(searchQs(Q,id)==-1)
    {
        printf("Error Queue not found !");
        return;
    }
    char fname[5];
    itoa(id,fname,10);
    int index = searchQs(Q,id);

    initSize = Q->Q[index].cSize;
    char data[initSize];

    Len = Q->Q[index].size;

    char fileName [20];
    snprintf(fileName,20,"%s%s%s",prefix,fname,postfix);
    printf("%s\n",fileName);

    for (int i=0;i<Len;i++) // for loop puts all the messages in specified queue into an array.
    {
        int new = initSize - (Q->Q[index].cSize);
        strcpy (&data[new],Q->Q[index].front->item.msg);
        deQueue(Q,id);
    }

    deleteQ(Q,id);
    listQs(Q);


    FILE *ptr = fopen(fileName,"wb");//creates file at specified path and opens file for writing.
    if(ptr == NULL)
    {
        printf("Unable to create a file.");
        fclose(ptr);
        exit(EXIT_FAILURE);
    }

    fputs(data, ptr); //writes data to file
    fclose(ptr);// closes and saves
    printf("Persist Successful!\n");
}

void restoreQ(MsgQs_t *Q) {
    char filename[15];
    int id;
    char *ptr;
    int delim=0; //variable for storing the number of the delimiters found in string.

    printf("Enter the filename of the queue you wish to be restored.\n");
    printf("Please use filename format queue_id.dat\n");
    scanf("%s", filename);

    FILE *fp = fopen(filename, "r");
    if(fp == NULL)
    {
        printf("Filename doesn't exist.\n");
        return;
    }


    ptr = filename;

    while (*ptr) {
        if (isdigit(*ptr)) {
            id = (int) strtol(ptr, &ptr, 10);
        } else {
            ptr++;
        }
    }

    if (searchQs(Q, id) != -1) {
        printf("ID already exists!\n");
        return;
    }

    if(Q->noQ == MAX_QUEUES)//checking if max queues reached.
    {
        printf("Queues full . \n");
        return;
    }

    //restoring the Queue
    Q->Q[Q->noQ].idNo=id;
    Q->Q[Q->noQ].front=Q->Q[Q->noQ].rear=NULL;
    Q->Q[Q->noQ].size=0;
    Q->Q[Q->noQ].cSize=0;

    (Q->noQ)++;

    long fileSize = GetFileSize(fp);
    if(fileSize == -1)
    {
        fclose(fp);
        printf("Error in retrieving file size.\n");
        return;
    }

    char *inputString = malloc(fileSize);
    if(inputString == NULL)
    {
        printf("Malloc Fail!\n");
        fclose(fp);
        return;
    }

    rewind(fp);

    size_t numElementsRead = fread(inputString, 1,fileSize,fp);
    assert(numElementsRead == fileSize);
    fclose(fp);


    for(int i=0;i<strlen(inputString);i++)
    {
        if (inputString[i]=='\n')
        {
            delim++;
        }
    }

    //Writing individual strings to respective nodes.
    char *offset = inputString;
    char *found;
    Item new[delim];
    size_t bytesToWrite;

    for (int i =0; i<delim;i++)
    {

      found=strstr(offset, "\n");
        if(found == NULL)
        {
            break;
        }
        bytesToWrite = (size_t)(found - offset);//offset between string found and start of search.
        memcpy(new[i].msg,offset,bytesToWrite+1);
        new[i].msg[bytesToWrite+1]='\0';//inserting the null term.
        enQueue(Q,new[i],searchQs(Q,id));
        offset += bytesToWrite+1;
    }

     printf("Queue restored!\n");
free(inputString);

}

void traverseDisp(MsgQs_t *Q, int id)
{
    int index = searchQs(Q,id);
    int i = 0;
    Node * pnode = Q->Q[index].front;

    while (pnode != NULL)
    {
        printf("%d: %s",i,pnode->item.msg);
        pnode = pnode->next;
        i++;
    }
    printf("\n");
}

long GetFileSize(FILE *f)
{
    int ret = fseek(f, 0,SEEK_END);//setting fp to end of file.
    if(ret != 0)
    {
        printf("fseek() failed with error: %d\n", errno);
        return -1;
    }

    long fSize = ftell(f);//gives us offset and hence the size of the file.
    if(fSize == -1)
    {
        printf("ftell() failed with error: %d\n", errno);
        return -1;
    }

    return fSize;
}
