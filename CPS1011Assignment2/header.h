//
// Created by benja on 12/23/2019.
//

#ifndef CPS1011ASSIGNMENT2_HEADER_H
#define CPS1011ASSIGNMENT2_HEADER_H

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#define MAX_MSGS 10
#define MAX_MSG_LEN 40
#define MAX_QUEUES 10

typedef struct message{
    char msg[MAX_MSG_LEN];
}Item;

typedef struct node
{
    Item item;
    struct node * next;
}Node;

typedef struct queue {
    int idNo; //unique identifier for specific queue
    Node *front;
    Node *rear;
    int size;//number of items in queue
    int cSize;// number of characters in queue
}Queue;

 typedef struct msgQs_t {
    Queue Q[MAX_QUEUES];
     int noQ;
}MsgQs_t;

MsgQs_t* initialiseMsgQs();
MsgQs_t* unloadMsgQs(MsgQs_t *Q);
void createQ( MsgQs_t *new);
bool checkIDUnique(MsgQs_t *Q);
void listQs(MsgQs_t *Q);
void sortQsID(MsgQs_t *Q);
void deleteQ(MsgQs_t *delete,int id);
void sendMessage(MsgQs_t *Q);
void sendMessageBatch(MsgQs_t *Q);
void receiveMessages(MsgQs_t *Q);
void enQueue(MsgQs_t *Q, Item new,int id);
void deQueue(MsgQs_t *Q,int id);
void copyToNode(Item item, Node * pnode);
int searchQs(MsgQs_t *Q,int id);
void purgeQs(MsgQs_t *Q);


#endif //CPS1011ASSIGNMENT2_HEADER_H
